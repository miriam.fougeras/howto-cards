---
layout: page
permalink: /external/lab-hsa/shipment/
shortcut: lab:shipment
redirect_from:
  - /cards/lab-hsa:shipment
  - /external/cards/lab-hsa:shipment
  - /lab/shipment
  - /external/external/lab-hsa/shipment/
  - /cards/lab:shipment
  - /external/cards/lab:shipment
---

# Shipment of a biological or chemical sample

To send biological or chemical samples, two types of companies are used : Worldcourier for precious samples (expensive but premium service guaranteeing the integrity of your sample and provides the dry ice and the shipping box)  or the two traditional courier company Fedex and DHL for standard samples.  The request for a shipment has to be made via the [ticketing system](https://service.uni.lu/sp?id=sc_category&sys_id=ca67f2c1db683c905c72ef3c0b9619a8&catalog_id=09863281db683c905c72ef3c0b9619dc) and someone from the Biotech Support Team will make the request on the shipping company website. Using the ticketing system will make sure that there will always be someone taking care of the request, while the questions asked in the ticket will provide all the information required for the shipment organisation.


If you want to **send** a parcel, please send a ticket [here](https://service.uni.lu/sp?id=sc_cat_item&sys_id=9cf7f205db683c905c72ef3c0b9619cd&sysparm_category=ca67f2c1db683c905c72ef3c0b9619a8).


If you want to **received** a parcel and you have to provide the labels to the seller, please send a ticket [here](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6a3e4701dbe83c905c72ef3c0b961903&sysparm_category=ca67f2c1db683c905c72ef3c0b9619a8).

Sending dangerous goods is only possible for people having received the **IATA** (International Air Transportation Association) training. This is why only some people frol the support team are allowed to prepare all the documents. The lab safety officer or a trained person from the support team will review the information provided in the ticket and will make sure all the safety requirements are fullfiled (shipping temperature and nature of the goods).

## Packaging instruction
*Extract from IATA Packaging Instruction 650 (DGR-62-EN-PI650).*

The packaging must consist of three components:
1. a primary receptacle(s);
2. a secondary packaging; and
3. a rigid outer packaging.

Primary receptacles must be packed in secondary packagings in such a way that, under normal conditions of transport, they cannot break, be punctured or leak their contents into the secondary packaging. Secondary packagings must be secured in outer packagings with suitable cushioning material. Any leakage of the contents must not compromise the integrity of the cushioning material or of the outer packaging.

Packages must be prepared as follows:

### Sending liquid substances

1. The primary receptacle(s) must be leakproof and must not contain more than 1 L;
2. The secondary packaging must be leakproof;
3. If multiple fragile primary receptacles are placed in a single secondary packaging, they must be either individually wrapped or separated to prevent contact between them;
4. Absorbent material must be placed between the primary receptacle and the secondary packaging. The absorbent material, such as cotton wool, must be in sufficient quantity to absorb the entire contents of the primary receptacle(s) so that any release of the liquid substance will not compromise the integrity of the cushioning material or of the outer packaging;
5. The primary receptacle or the secondary packaging must be capable of withstanding, without leakage, an internal pressure of 95 kPa.
6. The outer packaging must not contain more than 4 L. This quantity excludes ice, dry ice or liquid nitrogen when used to keep specimens cold.

### Sending solid substances

1. The primary receptacle(s) must be siftproof and must not exceed the outer packaging weight limit;
2. The secondary packaging must be siftproof;
3. If multiple fragile primary receptacles are placed in a single secondary packaging, they must be either individually wrapped or separated to prevent contact between them;
4. Except for packages containing body parts, organs or whole bodies, the outer packaging must not contain more than 4 kg. This quantity excludes ice, dry ice or liquid nitrogen when used to keep specimens cold;
5. If there is any doubt as to whether or not residual liquid may be present in the primary receptacle during transport then a packaging suitable for liquids, including absorbent materials, must be used

## Shipment with Fedex or DHL

If your shipment is sensitive to temperature, we advise you to plan the shipment on a Monday or a Tuesday to avoid any delay due to the week-end.

If the destination is outside of Europe, use at least 10kg of dry-ice.

To avoid any shortage of dry-ice, we ask you to order dry-ice for any shipment required 5kg or more. You can do so in Quarks. Please plan your shipment accordingly to the delivery date of your dry-ice.

Make sure to fill every information in the ticket. If an information is missing, the shipment will be delayed.

If you are sending a GMO to the USA, it is mandatory to add the emergency procedure.

When the documents are ready, they will be attached in the ticket and you will have to print them. You can then put them in a plastic pouch, available in the MUF room (BT2) or at the support team desk (BT1)

## Shipment with World Courier

1.  Contact <bruops@worldcourier.be> and add <aa-lcsb@uni.lu> in CC to ask for a quotation. Describe the content of the package, ask that they provide dry-ice (if applicable) and their packaging. Indicate the date and the place of the pick-up.

2.  When you receive your quotation, send it to <aa-lcsb@uni.lu>. The support team will place it in Quarks so the PO can be created and sent to World Courier.

3.  Fill and sign the documents sent by World Courier (i. e. proforma invoice).

    -   Date: date of the shipment
    -   Shipper: you + BT1 or BT2 address
    -   Consignee: receiver address


4.  After they receive all the documents, World Courier can register the request and plan the shipment. World Courier will send all the documents via email. You will have to print them and put them in a plastic pouch, available in the MUF room (BT2) or at the support team desk (BT1)

